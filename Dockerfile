FROM debian:bullseye

RUN apt-get update && apt-get install -y \
    python3 \
    build-essential \
    cmake \
    gcc-arm-none-eabi \
    libnewlib-arm-none-eabi \
    git \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /root/
RUN git clone -b master https://github.com/raspberrypi/pico-sdk.git

WORKDIR /root/pico-sdk/
RUN git submodule update --init

ENV PICO_SDK_PATH=/root/pico-sdk/

RUN mkdir -p /root/group2/assign02
WORKDIR /root/group2

COPY CMakeLists.txt .
COPY assign02/* assign02/
COPY pico_sdk_import.cmake .

RUN mkdir build
WORKDIR /root/group2/build/

RUN cmake ..
RUN make

<pre>
Microprocessors Group2  

Project 2  

    Name                        email                     phone                 role   
Michael Cummins             micummin@tcd.ie             0851700405            Project Demonstration Owner     
Sean Fahey                  faheyse@tcd.ie              0858204254            Code Owner  
Anshkirat Dhanju            dhanjua@tcd.ie              0892058710            Project Workflow Owner
Cian Donovan                donovaci@tcd.ie             0873355893            Gitlab Workflow Owner
Oisín Gartlan               ogartlan@tcd.ie             0871918967            Project Documentation Owner


</pre>

# Setup

1. Set the environment variable `PICO_SDK_PATH` to the absolute path of the Raspberry Pi Pico SDK
2. `git clone git@gitlab.scss.tcd.ie:faheyse/microprocessors-group2.git`
3. `cd microprocessors-group2`
4. `mkdir build && cd build`
5. `cmake ..`
6. `make`

## Optional

Automatically set known symlinked path for Pi Pico serial using udev rule

`sudo cp etc/52-rpi-pico.rules /etc/udev/rules.d/`

`sudo udevadm control --reload-rules && sudo udevadm trigger`

Automatically start OpenOCD server when Pico Probe plugged in

`sudo cp etc/rpi_pico_openocd.service /etc/systemd/system/`

`sudo systemctl enable rpi_pico_openocd.service`
